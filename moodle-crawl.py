from bs4 import BeautifulSoup
import requests
import argparse
import re
import os
from urllib.parse import urlparse

cookie_filename = "./cookie.txt"

cookies = dict()
with open(cookie_filename, 'r') as cookie_file:
	cookie_content = cookie_file.read()
	for line in cookie_content.split("\n"):
		(k, v) = line.split("=", 1)
		cookies[k] = v

fetched_urls = set()

url = "http://localhost:8081/course/view.php?id=2"

base_url = urlparse(url)._replace(path="", query="").geturl()
print(base_url)

course_id = re.match(r'.+?view\.php\?id=(\d+)', url).group(1)
print("Fetching course with ID " + course_id)

export_folder = "./export/course-" + course_id

try:
	os.makedirs(export_folder)
except:
	pass

def crawl(url, indent):

	if url == None:
		return

	if url.find("course/view.php?id=") != -1 and url.find("course/view.php?id=" + str(course_id)) == -1:
		return

	if url.find("admin") != -1 or url.find("switchrole.php") != -1 or url.find("edit") != -1:
		return

	if url.find("redirect") != -1:
		return

	if url.find("/calendar/") != -1 or url.find("/message/") != -1 or url.find("/user/") != -1 or url.find("management.php") != -1 or url.find("/grade") != -1 or url.find("/report") != -1 or url.find("/cohort") != -1 or url.find("/forum") != -1 or url.find("export") != -1 or url.find("backup") != -1 or url.find("restore") != -1 or url.find("print") != -1 or url.find("badges") != -1 or url.find("reset") != -1:
		return

	if url.find("logout") != -1:
		return
	if url.startswith("#") or url == base_url:
		return

	if not url.startswith(base_url):
		return

	if url not in fetched_urls:
		fetched_urls.add(url)
	else:
		return

	if url.endswith("/"):
		url = url + "index.html"

	if url.endswith("?forcedownload=1"):
		url = url.replace("?forcedownload=1", "")

	print(indent + "crawling " + str(url))

	r = requests.get(url, cookies=cookies)
	content = r.text

	filename = url.replace(base_url, "")
	try:
		os.makedirs(export_folder + os.path.dirname(filename))
	except:
		pass
	with open(export_folder + filename, "w") as output_file:
		output_file.write(content)
		# print("Loaded " + str(len(content)) + " bytes from " + url)

	soup = BeautifulSoup(content, "html.parser")
	page = soup.find(id="page-wrapper")
	if page == None:
		return
	links = page.find_all("a")
	i = 0
	for link in links:
		link_href = link.get('href')
		crawl(link_href, indent + "-")

crawl(str(url), "")

