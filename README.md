# Moodle-ILIAS crawler

Crawler für Moodle- und ILIAS-Kurse.

## Docker-Testumgebung für Moodle und ILIAS

Wenn Docker auf dem System installiert ist, dann können jeweils Test-Instanzen beider Systeme mit dem Befehl

```
docker-compose up -d
```

gestartet werden.
Die Ausgaben der jeweiligen Container kann mit 

```
docker-compose logs -f
```

verfolgt werden.
Zu beachten ist, dass ein initiales Setup bis zu zehn Minuten dauern kann - also Geduld!
Anschließend sind die Instanzen für [Moodle](http://localhost:8081) und [ILIAS](http://localhost:8080) erreichbar.

Die Standardzugangsdaten sind für beide Systeme jeweils

**Username:** `root`
**Passwort:** `password`