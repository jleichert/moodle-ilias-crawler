from bs4 import BeautifulSoup
import re
import requests
import os

# Start the session
session = requests.Session()

# Create the payload
payload = {'username': 'benno',
           'password': 'Demo4ilias',
           'cmd[doStandardAuthentication]': 'Login'
           }

# Post the payload to the site to log in
s = session.post(
    "https://demo.ilias.de/ilias.php?lang=en&client_id=demo&cmd=post&cmdClass=ilstartupgui&cmdNode=aj&baseClass=ilStartUpGUI&rtoken=", data=payload)


def crawl(url, session, visited_links):
    res = session.get(url)
    soup = BeautifulSoup(res.content, "lxml")
    visited_links.append(url)
    if "goto" in str(url):
        ids = re.findall(r'\d+', str(url))
        if len(ids) > 0:
            filename = 'course-' + ids[0]
            write_to_file(res.text, filename)
            print("saving" + filename)
    for link in soup.findAll('a'):
        href_link = link.get('href')
        if "http" in str(href_link) and href_link not in visited_links:
            visited_links.append(href_link)
            crawl(link.get('href'), session, visited_links)


def write_to_file(text, filename):
    export_folder = "./ilias/content/"
    try:
        os.makedirs(export_folder + os.path.dirname(filename))
    except os.error:
        pass
    with open(export_folder + filename, "w") as output_file:
        output_file.write(text)


def main():
    scenarios = "https://demo.ilias.de/ilias.php?ref_id=227&cmd=frameset&cmdClass=ilrepositorygui&cmdNode=57&baseClass=ilRepositoryGUI"
    visited_links = []
    crawl(scenarios, session, visited_links)


if __name__ == "__main__":
    main()
